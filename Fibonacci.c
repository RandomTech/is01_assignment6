#include<stdio.h>
int fibonacciSeq(int n){
	if(n<3) return 1;
	else return fibonacciSeq(n-1)+ fibonacciSeq(n-2);
}
int main(){
	int i,num;
	printf("Enter Fibonacci Sequence limit: \n");
	scanf("%d", &num);
	
	for(i=1; i<=num; i++){
		printf("%d\n", fibonacciSeq(i));
	}
	return 0;
}